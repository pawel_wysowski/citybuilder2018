﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CityStatsUI : MonoBehaviour {

    public TextMeshProUGUI citizensStat;
    public TextMeshProUGUI hapinessStat;
    public TextMeshProUGUI powerShortageStat;
    public TextMeshProUGUI dateStat;
    public TextMeshProUGUI moneyStat;

    private void OnEnable()
    {
        GameManagerMaster.OnMoneyChanged += SetMoneyStat;
        GameManagerCitizens.OnCitizenStatChanged += SetCitizenStat;
        GameManagerHappiness.OnHappinessStatChanged += SetHappinessStat;
        GameManagerBuilding.OnBuildingsWithoutPowerChange += SetPowerShortageStat;
        GameManagerTime.MonthEventChange += SetDateStat;
    }

    private void OnDisable()
    {
        GameManagerMaster.OnMoneyChanged -= SetMoneyStat;
        GameManagerCitizens.OnCitizenStatChanged -= SetCitizenStat;
        GameManagerHappiness.OnHappinessStatChanged -= SetHappinessStat;
        GameManagerBuilding.OnBuildingsWithoutPowerChange -= SetPowerShortageStat;
        GameManagerTime.MonthEventChange -= SetDateStat;
    }

    public void SetCitizenStat(int citizens, int employed = 0, int unemployed = 0)
    {
        citizensStat.SetText( string.Format("{0}", citizens));
    }

    public void SetHappinessStat(int happiness)
    {
        hapinessStat.SetText(string.Format("{0}", happiness));
    }

    public void SetPowerShortageStat(int buildingsWithoutPower)
    {
        powerShortageStat.SetText(string.Format("{0}", buildingsWithoutPower));
    }

    public void SetMoneyStat(int money)
    {
        moneyStat.SetText(string.Format("{0}", money));
    }

    public void SetDateStat(int year, int month)
    {
        dateStat.SetText(string.Format("Y: {0} M: {1}", year, month));
    }
}
