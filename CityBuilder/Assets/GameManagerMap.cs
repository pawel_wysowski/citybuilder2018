﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerMap : MonoBehaviour
{
    public GameObject MapLvl2;
    public GameObject MapLvl3;
    public GameObject MapLvl4;
    public GameObject MapLvl5;
    public GameObject MapLvl6;
    public GameObject MapLvl7;


    public delegate void GameManagerMapEventHandler();
    public static event GameManagerMapEventHandler OnMapUnlocked;

    private void OnEnable()
    {
        QuestManager.OnLevelUp += EnableMap;
    }

    private void OnDisable()
    {
        QuestManager.OnLevelUp -= EnableMap;
    }

    private void EnableMap(int lvl)
    {
        Transform Map = null;
        switch (lvl)
        {
            case 1:
                MapLvl2.SetActive(true);
                Map = MapLvl2.transform;
                break;
            case 2:
                MapLvl3.SetActive(true);
                Map = MapLvl3.transform;
                break;
            case 3:
                MapLvl4.SetActive(true);
                Map = MapLvl4.transform;
                break;
            case 4:
                MapLvl5.SetActive(true);
                Map = MapLvl5.transform;
                break;
            case 5:
                MapLvl6.SetActive(true);
                Map = MapLvl6.transform;
                break;
            case 6:
                MapLvl7.SetActive(true);
                Map = MapLvl7.transform;
                break;
        }
        OnMapUnlocked?.Invoke();

    }
}
