﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class TipUI : MonoBehaviour {
    public TextMeshProUGUI specialCharacter;
    public TextMeshProUGUI tip;
    public Animator animator;

    private void OnEnable()
    {
        Tile.PowerShortage += Tile_PowerShortage;
        BuildingMenuUI.MoneyShortage += BuildingMenu_UI_MoneyShortage;
        Quest.OnQuestComplete += Quest_OnQuestComplete;
        QuestManager.OnLevelUp += QuestManager_OnLevelUp;
    }

    private void QuestManager_OnLevelUp(int lvl)
    {
        ShowUI("New parcels added to your city!");
    }

    private void Quest_OnQuestComplete(Quest quest)
    {
        ShowUI("Quest " + quest.QuestName + " completed!");
    }

    private void BuildingMenu_UI_MoneyShortage()
    {
        ShowUI("You do not have enough money");
    }

    private void OnDisable()
    {
        Tile.PowerShortage -= Tile_PowerShortage;
        Quest.OnQuestComplete -= Quest_OnQuestComplete;
        BuildingMenuUI.MoneyShortage -= BuildingMenu_UI_MoneyShortage;
        QuestManager.OnLevelUp -= QuestManager_OnLevelUp;
    }

    public void ToggleShowUI()
    {
        animator.SetBool("Show", false);
    }

    public void ToggleHide()
    {
        animator.SetBool("Hide", false);
    }

    private void Tile_PowerShortage(Tile tile)
    {
        ShowUI("One of your building has not enough power");
    }

    public void ShowUI(string message)
    {
        specialCharacter.SetText("!");
        animator.SetBool("Show", true);
        SetTipText(message);
        Invoke("HideUI", 3f);
    }

    private void SetTipText(string message)
    {
        specialCharacter.gameObject.SetActive(true);
        tip.SetText(message);
    }

    public void HideUI()
    {
        specialCharacter.SetText("");
        SetTipText("");
        animator.SetBool("Hide", true);
    }
}
