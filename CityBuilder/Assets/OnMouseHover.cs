﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnMouseHover : MonoBehaviour
{
    private GameObject Info;

    private void Awake()
    {
        Info = transform.GetChild(transform.childCount - 1).gameObject;
    }

   public void DisplayInfo()
    {
        Info.SetActive(true);
    }

    public void HideInfo()
    {
        Info.SetActive(false);
    }
}
