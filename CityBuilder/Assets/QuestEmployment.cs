﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestEmployment : Quest
{
    public GameManagerCitizens gameManager_Citizens;
    public float percentage;
    private bool shouldCheck;
    private GameObject QuestViewInst;

    public override void CompleteQuest()
    {
        CallQuestCompletedEvent();
        IsCompleted = true;
        QuestViewInst.GetComponent<QuestView>().SetQuestCompleteLabel();
    }

    public override void DestroyView()
    {
        Destroy(QuestViewInst);
    }

    public override void StartQuest()
    {
        shouldCheck = true;
        QuestViewInst = Instantiate(QuestView, QuestUI);
        QuestViewInst.GetComponent<QuestView>().ShowQuestView(this);
    }

    private void CheckObjective()
    {
        if (gameManager_Citizens._allEmployed >= gameManager_Citizens.AllCitizens * percentage*0.01f)
        {
            CompleteQuest();
            shouldCheck = false;
        }
    }

    void Update()
    {
        if (shouldCheck)
        {
            CheckObjective();
        }
    }
}
