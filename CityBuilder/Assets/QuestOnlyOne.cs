﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestOnlyOne : Quest
{
    [SerializeField]
    private GameManagerBuilding gameManager_building;
    private GameObject QuestViewInst;
    private int buildingCounter;

    public int buildingCountObjective = 1;
    public BuildingTypeEnum buildingObjective;
    private bool shouldCheckObjective;

    public override void CompleteQuest()
    {
        IsCompleted = true;
        shouldCheckObjective = false;
        CallQuestCompletedEvent();
        QuestViewInst.GetComponent<QuestView>().SetQuestCompleteLabel();
    }

    public override void StartQuest()
    {
        QuestViewInst = Instantiate(QuestView, QuestUI);
        QuestViewInst.GetComponent<QuestView>().ShowQuestView(this);
        shouldCheckObjective = true;
    }

    private void CheckObjective()
    {
        var powerplants = gameManager_building.Powerplants.FindAll(x => x.GetComponent<BuildingCoalPowerplant>());
        if ( powerplants.Count.Equals(1) && gameManager_building.BuildingsWithoutPower.Equals(0))
        {
            CompleteQuest();
        }
    }

    public override void DestroyView()
    {
        Destroy(QuestViewInst);
    }

    private void Update()
    {
        if (shouldCheckObjective)
        {
            CheckObjective();
        }
    }
}
