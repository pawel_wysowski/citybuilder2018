﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TileGrass : Tile
{
    public List<Material> OutlineMaterials;
    public List<Material> PreviousMaterials = new List<Material>(2);
    void OnEnable()
    {
        base.tileType = TileTypeEnum.grass;
    }
    public override void ClickInteraction()
    {
        base.ClickInteraction();
        if (GetBuilding() == null)
        {
            base.pointerOverTile = false;
            base.HoverInteraction();
            base.CallOnTileClicked(this);
        }
        else
        {
            CallBuildingClicked(GetBuilding());
        }

    }

    private void CallBuildingClicked(Building building)
    {
        CallHouseClicked(building);
    }
}
