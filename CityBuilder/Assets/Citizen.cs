﻿using System;
[System.Serializable]
public class Citizen
{
    public bool haveJob;
    public IHaveEmployees job;
    public BuildingHousing house;

    public Citizen(bool haveJob, IHaveEmployees job, BuildingHousing house)
    {
        this.haveJob = haveJob;
        this.job = job;
        this.house = house;
    }

    public void DismissFromWork()
    {
        if(job!=null)
        job.Dismiss(this);
    }
}