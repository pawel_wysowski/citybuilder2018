﻿using cakeslice;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Tile : MonoBehaviour
{
    public TileTypeEnum tileType;
    public BuildingTypeEnum buildingOnTile;
    private GameObject building;

    public delegate void TileEventHandler();
    public static event TileEventHandler OnHappinessChanged;
    public static event TileEventHandler OnPowerChanged;

    public float _power = 0;
    [SerializeField] private int _happiness = 0;

    public int Happiness
    {
        get { return _happiness; }
        set
        {
            _happiness = value;
            CallHappinessChange();
        }
    }

    private void CallHappinessChange()
    {
        if (OnHappinessChanged != null)
        {
            OnHappinessChanged();
        }
    }

    public float power
    {
        get { return _power; }
        set
        {
            _power = value;
            CheckBuildingPower();
        }
    }

    private void CheckBuildingPower()
    {
        if (building)
        {
            if (building.GetComponent<IHavePowerNeeds>() != null)
            {
                if (power >= building.GetComponent<IHavePowerNeeds>().powerNeeds)
                {
                    building.GetComponent<IHavePowerNeeds>().hasPower = true;
                }
                else
                {
                    building.GetComponent<IHavePowerNeeds>().hasPower = false;
                }
                building.GetComponent<Building>().gameManagerBuilding.CheckBuildingsWithoutPower();
            }
        }
        
    }

    public Building GetBuilding()
    {
        if (building)
            return building.GetComponent<Building>();
        else
            return null;
    }

    protected bool pointerOverTile;
    private Color previous;
    private Color hoverColor = new Color(0.1221f, 0.2547f, 0.1069f);

    public delegate void TileInteractionHandler(Tile tile);
    public static event TileInteractionHandler OnTileClicked;
    public static event TileInteractionHandler PowerShortage;

    public delegate void TileInteractionHandlerBuilding(Building building);
    public static event TileInteractionHandlerBuilding OnBuildingClicked;

    public virtual void ClickInteraction()
    {
    }
    public virtual void HoverInteraction()
    {
        if (!pointerOverTile)
        {
            previous = GetComponent<MeshRenderer>().material.GetColor("_Color");
            GetComponent<MeshRenderer>().material.SetColor("_Color", hoverColor);
            pointerOverTile = true;
        }
    }
    public virtual void HoverInteractionEnd()
    {
        if (pointerOverTile)
        {
            GetComponent<MeshRenderer>().material.SetColor("_Color", previous);
            pointerOverTile = false;
        }

    }
    public virtual void Build(Building building)
    {
        var newBuilding = Instantiate(building, transform);
        buildingOnTile = building.buildingType;
        newBuilding.tile = this;
        this.building = newBuilding.gameObject;
        newBuilding.RegisterBuilding();
        ApplyBuildingEffects(newBuilding);
    }

    public virtual void DestroyBuilding(Building building)
    {
        buildingOnTile = BuildingTypeEnum.None;
        building.UnregisterBuilding();
        ApplyBuildingEffectsOnDestroy(building);
        this.building = null;
    }

    public virtual void ApplyBuildingEffects(Building building)
    {
        building.ApplyBuildingModifiersForOneTile(this);
        if (power < 0)
            CallPowerShortage();

        var neighbours = Physics.OverlapSphere(this.transform.position, 2.5f, 512);
        if (neighbours != null)
        {
            foreach (Collider tile in neighbours)
            {
                if (tile != GetComponent<Collider>())
                    building.ApplyBuildingModifiers(tile.GetComponent<Tile>());
            }
        }
    }

    public virtual void ApplyBuildingEffectsOnDestroy(Building building)
    {
        var neighbours = Physics.OverlapSphere(this.transform.position, 2.5f, 512);
        if (neighbours != null)
        {
            foreach (Collider tile in neighbours)
            {
                if (tile != GetComponent<Collider>())
                    building.ApplyBuildingModifiersOnDestroy(tile.GetComponent<Tile>());
            }
        }
    }

    private void CallPowerShortage()
    {
        if (PowerShortage != null)
        {
            PowerShortage(this);
        }
    }

    public void CallOnTileClicked(Tile tile)
    {
        if (OnTileClicked != null)
        {
            OnTileClicked(tile);
        }
    }

    public void CallHouseClicked(Building building)
    {
        OnBuildingClicked?.Invoke(building);
    }
}
