﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuView : MonoBehaviour, IMenuView
{
    public bool visible;
    public GameObject menuPanel;
    public Button continueBtn;
    public Button exitBtn;
    [SerializeField]
    private GameManagerMaster gameManagerMaster;

    public void Hide()
    {
        menuPanel.SetActive(false);
        continueBtn.onClick.RemoveAllListeners();
        exitBtn.onClick.RemoveAllListeners();
        visible = false;
    }

    public void HideMenu()
    {
        gameManagerMaster.TogglePause();
    }

    public void ShowMenu()
    {
        gameManagerMaster.Push(this);
        continueBtn.onClick.AddListener(gameManagerMaster.Pop);
        exitBtn.onClick.AddListener(gameManagerMaster.ExitGame);
        menuPanel.SetActive(true);
        visible = true;
    }

    public void Show()
    {
        ShowMenu();
    }
}