﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingPowerRepeater : Building
{
    public float powerSupply;

    public override void RegisterBuilding()
    {
        base.buildingType = BuildingTypeEnum.PowerRepeater;
        base.HappinessModifier = -5;
        base.CallOnBuildingBuild(gameObject);
        powerSupply = tile.power;
        GameManagerMap.OnMapUnlocked += RedistributePower;
    }

    public void RedistributePower()
    {
        powerSupply = tile.power;
        var neighbours = Physics.OverlapSphere(this.tile.transform.position, 2.5f, 512);
        if (neighbours != null)
        {
            foreach (Collider tile in neighbours)
            {
                if (tile != this.tile.GetComponent<Collider>())
                {
                    if (!tile.GetComponent<Tile>().GetBuilding())
                        tile.GetComponent<Tile>().power += powerSupply;
                }
            }
        }
    }
    public override void ApplyBuildingModifiers(Tile tile)
    {
        tile.power += powerSupply;
        tile.Happiness += base.HappinessModifier;
    }

    public override void ApplyBuildingModifiersForOneTile(Tile tile)
    {
    }

    public override void UnregisterBuilding()
    {
        GameManagerMap.OnMapUnlocked -= RedistributePower;
        base.CallOnBuildingDestroyed(gameObject);
      //  Destroy(gameObject);
    }

    public override void ApplyBuildingModifiersOnDestroy(Tile tile)
    {
        tile.Happiness -= base.HappinessModifier;
        tile.power -= powerSupply;
    }
}
