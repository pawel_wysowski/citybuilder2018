﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingPark : Building
{
    public override void ApplyBuildingModifiers(Tile tile)
    {
        tile.Happiness += HappinessModifier;
    }

    public override void ApplyBuildingModifiersForOneTile(Tile tile)
    {
        
    }

    public override void ApplyBuildingModifiersOnDestroy(Tile tile)
    {
        tile.Happiness -= HappinessModifier;
    }

    public override void RegisterBuilding()
    {
        base.buildingType = BuildingTypeEnum.Parks;
        base.HappinessModifier = 20;
        CallOnBuildingBuild(gameObject);
    }

    public override void UnregisterBuilding()
    {
        base.CallOnBuildingDestroyed(gameObject);
       // Destroy(gameObject);
    }
}
