﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestMoney : Quest
{
    public GameManagerMaster gameManager_Master;
    public float moneyObjective;
    private bool shouldCheck;
    private GameObject QuestViewInst;

    public override void CompleteQuest()
    {
        CallQuestCompletedEvent();
        IsCompleted = true;
        QuestViewInst.GetComponent<QuestView>().SetQuestCompleteLabel();
    }

    public override void DestroyView()
    {
        Destroy(QuestViewInst);
    }

    public override void StartQuest()
    {
        shouldCheck = true;
        QuestViewInst = Instantiate(QuestView, QuestUI);
        QuestViewInst.GetComponent<QuestView>().ShowQuestView(this);
    }

    private void CheckObjective()
    {
        if (gameManager_Master.money >= moneyObjective)
        {
            CompleteQuest();
            shouldCheck = false;
        }
    }

    void Update()
    {
        if (shouldCheck)
        {
            CheckObjective();
        }
    }
}
