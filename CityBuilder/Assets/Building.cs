﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Building : MonoBehaviour {
    public BuildingTypeEnum buildingType;
    public GameManagerBuilding gameManagerBuilding;
    public GameObject buildingPrefab;
    public Tile tile;
    public int HappinessModifier;
    public int cost;
    public delegate void BuildingEventHandler(GameObject building);
    public static event BuildingEventHandler OnBuildingEnabled;
    public static event BuildingEventHandler OnBuildingDisabled;

    public abstract void ApplyBuildingModifiers(Tile tile);
    public abstract void ApplyBuildingModifiersForOneTile(Tile tile);
    public abstract void ApplyBuildingModifiersOnDestroy(Tile tile);
    public abstract void RegisterBuilding();
    public abstract void UnregisterBuilding();

    public void CallOnBuildingBuild(GameObject building)
    {
        OnBuildingEnabled?.Invoke(building);
    }

    public void CallOnBuildingDestroyed(GameObject building)
    {
        GetComponent<Animator>().SetBool("destroyBuilding", true);
        OnBuildingDisabled?.Invoke(building);
    }

    private void OnEnable()
    {
        DestroyAnimationEventTrigger.OnAnimationEnd += DestroyThisBuilding;
    }

    private void OnDisable()
    {
        DestroyAnimationEventTrigger.OnAnimationEnd -= DestroyThisBuilding;
    }

    private void DestroyThisBuilding()
    {
        Destroy(gameObject);
    }
}
