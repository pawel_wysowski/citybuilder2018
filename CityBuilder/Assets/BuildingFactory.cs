﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingFactory : Building, IHavePowerNeeds, IHaveEmployees
{
    private bool _hasPower;
    private float powerNeed = 20;
    private int maxEmployees = 5;
    private int currentEmployees = 0;
    public List<Citizen> Employees = new List<Citizen>();
    public bool hasPower
    {
        get
        {
            return _hasPower;
        }

        set
        {
            if (_hasPower != value)
            {
                _hasPower = value;
            }
        }
    }
    public float powerNeeds
    {
        get
        {
            return powerNeed;
        }
        set
        {
            if (powerNeed != value)
                powerNeed = value;
        }
    }

    public int MaxEmployees { get { return maxEmployees; } set { if (maxEmployees != value) maxEmployees = value; } }
    public int MonthlyIncomeForEmployee { get { return 0; } }

    public int CurrentEmployees { get { return currentEmployees; } set { if (currentEmployees != value) currentEmployees = value; } }

    public override void ApplyBuildingModifiers(Tile tile)
    {
        tile.Happiness -= 5;
        FindEmployees(tile);
    }

    private void FindEmployees(Tile tile)
    {
        if (tile.GetBuilding())
        {
            if (tile.GetBuilding().GetComponent<BuildingHousing>())
            {
                tile.GetBuilding().GetComponent<BuildingHousing>().UnemployedCitizens(this);
            }
        }
    }

    public override void ApplyBuildingModifiersForOneTile(Tile tile)
    {
        tile.power -= powerNeeds;
    }

    public override void RegisterBuilding()
    {
        hasPower = false;
        base.buildingType = BuildingTypeEnum.Industries;
        base.HappinessModifier = 5;
        Debug.Log(Employees.Count);
        Employees.Clear();
        Employees.TrimExcess();
        CallOnBuildingBuild(gameObject);
        BuildingHousing.OnCitizensChanged += FindNewEmployees;
    }

    public override void UnregisterBuilding()
    {
        hasPower = false;
        BuildingHousing.OnCitizensChanged -= FindNewEmployees;
        FireAllEmployees();
        CallOnBuildingDestroyed(gameObject);
        //Destroy(gameObject);
    }

    private void FireAllEmployees()
    {
        foreach (Citizen worker in Employees)
        {
            worker.haveJob = false;
            worker.job = null;
        }
        Employees.Clear();
    }

    public void Dismiss(Citizen citizen)
    {
        citizen.haveJob = false;
        citizen.job = null;
        Employees.Remove(citizen);
        Employees.TrimExcess();
        CurrentEmployees = Employees.Count;
        citizen = null;
    }

    public void HireEmployee(Citizen citizen)
    {
        Debug.Log(MaxEmployees);
        if (Employees.Count < MaxEmployees)
        {
            citizen.haveJob = true;
            citizen.job = this;
            Employees.Add(citizen);
        }
        CurrentEmployees = Employees.Count;
        Debug.Log(Employees.Count);
    }

    public void FindNewEmployees()
    {
        var neighbours = Physics.OverlapSphere(this.transform.position, 2.5f, 512);
        if (neighbours != null)
        {
            foreach (Collider tile in neighbours)
            {
                if (tile != this.tile.GetComponent<Collider>())
                {
                    if (tile.GetComponent<Tile>().GetBuilding())
                    {
                        if (tile.GetComponent<Tile>().GetBuilding().GetComponent<BuildingHousing>())
                                tile.GetComponent<Tile>().GetBuilding().GetComponent<BuildingHousing>().UnemployedCitizens(this);
                    }
                }
            }
        }
    }

    public override void ApplyBuildingModifiersOnDestroy(Tile tile)
    {
    }
}
