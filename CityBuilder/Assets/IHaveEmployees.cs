﻿public interface IHaveEmployees
{
    int MaxEmployees { get; set; }
    int MonthlyIncomeForEmployee { get; }
    int CurrentEmployees { get; set; }

    void HireEmployee(Citizen citizen);
    void Dismiss(Citizen citizen);
}