﻿using UnityEngine;
using System.Collections;
using TMPro;

public class QuestView : MonoBehaviour
{
    public TextMeshProUGUI QuestNameLabel;
    public TextMeshProUGUI QuestDescription;

    public void SetQuestNameLabel(string name)
    {
        QuestNameLabel.SetText(name);
    }

    public void SetQuestDescriptionLabel(string description)
    {
        QuestDescription.SetText(description);
    }

    public void SetQuestCompleteLabel()
    {
        QuestNameLabel.fontStyle = FontStyles.Strikethrough;
        QuestDescription.fontStyle = FontStyles.Strikethrough;
    }

    public void SetQuestUncompleteLabel()
    {
        QuestNameLabel.fontStyle = FontStyles.Normal;
        QuestDescription.fontStyle = FontStyles.Normal;
    }

    public void ShowQuestView(Quest quest)
    {
        SetQuestNameLabel(quest.QuestName);
        SetQuestDescriptionLabel(quest.QuestDescription);
        SetQuestUncompleteLabel();
    }
}
