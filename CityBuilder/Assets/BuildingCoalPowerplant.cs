﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingCoalPowerplant : Building {
    public float powerSupply;

    public override void RegisterBuilding()
    {
        base.buildingType = BuildingTypeEnum.PowerPlants;
        base.HappinessModifier = -10;
        base.CallOnBuildingBuild(gameObject);
        GameManagerMap.OnMapUnlocked += RedistributePower;
    }

    private void RedistributePower()
    {
        var neighbours = Physics.OverlapSphere(this.tile.transform.position, 2.5f, 512);
        if (neighbours != null)
        {
            foreach (Collider tile in neighbours)
            {
                if (tile != this.tile.GetComponent<Collider>())
                {
                    if(!tile.GetComponent<Tile>().GetBuilding())
                    tile.GetComponent<Tile>().power += powerSupply;
                }
            }
        }
    }

    public override void ApplyBuildingModifiers(Tile tile)
    {
        tile.power += powerSupply;
        tile.Happiness += base.HappinessModifier;

        if (tile.GetBuilding())
        {
            if (tile.GetBuilding().GetComponent<BuildingPowerRepeater>())
            {
                tile.GetBuilding().GetComponent<BuildingPowerRepeater>().RedistributePower();
            }
        }
    }

    public override void ApplyBuildingModifiersForOneTile(Tile tile)
    {
    }

    public override void UnregisterBuilding()
    {
        GameManagerMap.OnMapUnlocked -= RedistributePower;
        base.CallOnBuildingDestroyed(gameObject);
       // Destroy(gameObject);
    }

    public override void ApplyBuildingModifiersOnDestroy(Tile tile)
    {
        tile.Happiness -= base.HappinessModifier;
        tile.power -= powerSupply;
    }
}
