﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HouseStatsUI : MonoBehaviour, IMenuView
{
    public GameObject VerticalLayoutGroup;
    public TextMeshProUGUI InhabitantsLabel;
    public TextMeshProUGUI UnemployedInhabitantsLabel;
    public TextMeshProUGUI EmployedInhabitantsLabel;
    public Button backButton;
    public Button destroyButton;
    private Sprite image;
    private Building _building;
    [SerializeField] private GameManagerMaster gameManager;
    private void OnEnable()
    {
        GameManagerBuilding.OnHouseTileClicked += Show;
    }


    private void Show(Building building)
    {
        gameManager.Push(this);
        _building = building;
        backButton.onClick.RemoveAllListeners();
        backButton.onClick.AddListener(gameManager.Pop);
        destroyButton.onClick.AddListener(DestroyBuilding);
        ShowMenu();
    }

    private void DestroyBuilding()
    {
        if (_building)
        {
            _building.tile.DestroyBuilding(_building);
            gameManager.Pop();
        }
    }

    private void Hide()
    {
        VerticalLayoutGroup.SetActive(false);
        GetComponent<Image>().enabled = false;
    }

    public void SetInhabitantsLabel(int number)
    {
        InhabitantsLabel.SetText(string.Format("Inhabitants: {0}", number));
    }

    public void SetUnemployedInhabitantsLabel(int number)
    {
        UnemployedInhabitantsLabel.SetText(string.Format("Unemployed: {0}", number));
    }

    public void SetEmployedInhabitantsLabel(int number)
    {
        EmployedInhabitantsLabel.SetText(string.Format("Employed: {0}", number));
    }

    public void ShowMenu()
    {
        var house = (BuildingHousing)_building;
        SetInhabitantsLabel(house.Citizens);
        SetEmployedInhabitantsLabel(house.EmployedCitizensCount());
        SetUnemployedInhabitantsLabel(house.UnemployedCitizensCount());
        GetComponent<Image>().enabled = true;
        VerticalLayoutGroup.SetActive(true);
    }

    public void HideMenu()
    {
        _building = null;
        Hide();
    }
}
