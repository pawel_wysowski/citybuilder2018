﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerHappiness : MonoBehaviour
{


    GameManagerBuilding gameManagerBuildings;
    GameManagerMaster gameManagerMaster;
    private int _allHappiness = 0;

    public delegate void GameManagerHappinessHandler(int happiness);
    public static event GameManagerHappinessHandler OnHappinessStatChanged;

    public int AllHappiness
    {
        get { return _allHappiness; }
        set
        {
            _allHappiness = value;
            CallOnCitizenStatChanged();
        }
    }

    private void CallOnCitizenStatChanged()
    {
        if (OnHappinessStatChanged != null)
        {
            OnHappinessStatChanged(AllHappiness);
        }
    }

    private void OnEnable()
    {
        gameManagerBuildings = GetComponent<GameManagerBuilding>();
        gameManagerMaster = GetComponent<GameManagerMaster>();
        Tile.OnHappinessChanged += GetAllHappiness;
        gameManagerMaster.OnMonthPassedEvent += MonthPassed;
    }


    private void OnDisable()
    {
        Tile.OnHappinessChanged -= GetAllHappiness;
        gameManagerMaster.OnMonthPassedEvent -= MonthPassed;
    }

    private void MonthPassed()
    {
        GetAllHappiness();
    }

    private void GetAllHappiness()
    {
        int happiness = 0;
        foreach (GameObject h in gameManagerBuildings.HousesOnMap)
        {
            var house = h.GetComponent<BuildingHousing>();
            happiness += house.tile.Happiness;
        }
        if (happiness > 0)
            AllHappiness = happiness / gameManagerBuildings.HousesOnMap.Count;
        else
            AllHappiness = 0;
    }

}
