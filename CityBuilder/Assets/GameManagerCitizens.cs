﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManagerCitizens : MonoBehaviour
{

    GameManagerBuilding gameManagerBuildings;
    GameManagerMaster gameManagerMaster;
    private int _allcitizens = 0;
    public int _allUnemployed = 0;
    public int _allEmployed = 0;
    public delegate void GameManagerCitizensHandler(int citizens, int umeployed, int employed);
    public static event GameManagerCitizensHandler OnCitizenStatChanged;

    public int AllCitizens
    {
        get { return _allcitizens; }
        set
        {
            _allcitizens = value;
            CallOnCitizenStatChanged();
        }
    }

    private void CallOnCitizenStatChanged()
    {
        OnCitizenStatChanged?.Invoke(AllCitizens, _allUnemployed, _allEmployed);
    }

    private void OnEnable()
    {
        gameManagerBuildings = GetComponent<GameManagerBuilding>();
        gameManagerMaster = GetComponent<GameManagerMaster>();
        BuildingHousing.OnCitizensChanged += GetAllCitizensFromHouses;
        gameManagerMaster.OnMonthPassedEvent += MonthPassed;
    }


    private void OnDisable()
    {
        BuildingHousing.OnCitizensChanged -= GetAllCitizensFromHouses;
        gameManagerMaster.OnMonthPassedEvent -= MonthPassed;
    }

    private void MonthPassed()
    {
        GetTaxMoney();
        GetMoneyFromWorkplaces();
        GetAllCitizensFromHouses();
    }

    private void GetMoneyFromWorkplaces()
    {
        int AllEmployees = 0;
        foreach (GameObject w in gameManagerBuildings.WorkPlaces)
        {
            if (w.GetComponent<IHavePowerNeeds>().hasPower)
            {
                var workplace = w.GetComponent<IHaveEmployees>();
                AllEmployees += workplace.CurrentEmployees;
            }
        }
        gameManagerMaster.money += AllEmployees * 50;
    }

    private void GetTaxMoney()
    {
        foreach (GameObject h in gameManagerBuildings.HousesOnMap)
        {
            var house = h.GetComponent<BuildingHousing>();
            if (house.Citizens > 0)
            {
                gameManagerMaster.money += house.Citizens;
            }

            if (GetComponent<GameManagerHappiness>().AllHappiness >= 10)
            {
                house.MoveIn();
            }
            else
            { 
                house.MoveOut();
            }
        }
    }

    private void GetAllCitizensFromHouses()
    {
        int citizens = 0;
        int employed = 0;
        int unemployed = 0;
        foreach (GameObject h in gameManagerBuildings.HousesOnMap)
        {
            var house = h.GetComponent<BuildingHousing>();
            citizens += house.AllCitizensCount();
            employed += house.EmployedCitizensCount();
            unemployed += house.UnemployedCitizensCount();
        }
        AllCitizens = citizens;
        _allEmployed = employed;
        _allUnemployed = unemployed;
    }

}
