﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GameManagerTime : MonoBehaviour, IGameManager {

    private GameManagerMaster gameManagerMaster;
    private bool isGameRunning;
    private float gameTime = 0;
    private int months = 0;
    private int years = 0;
    private float timeModifier;

    public delegate void GameManagerTimeEventHandler(int year, int month);
    public static event GameManagerTimeEventHandler MonthEventChange;

    public void SetInitialReferences()
    {
        gameManagerMaster = GetComponent<GameManagerMaster>();
    }

    void OnEnable()
    {
        SetInitialReferences();
        gameManagerMaster.OnGameStartEvent += GameManagerMaster_OnGameStartEvent;
    }

    void OnDisable()
    {
        gameManagerMaster.OnGameStartEvent += GameManagerMaster_OnGameStartEvent;
    }

    private void GameManagerMaster_OnGameStartEvent()
    {
        isGameRunning = true;
    }

    private void CallMonthEventChange()
    {
        MonthEventChange?.Invoke(years, months);
    }
	
    public int GetMonth()
    {
        int monthsToReturn = months + years * 12;
        return monthsToReturn;
    }

	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.Space))
        {
            timeModifier = 20;
        }
        else
        {
            timeModifier = 1;
        }


        if (isGameRunning)
        {
            gameTime += Time.deltaTime*timeModifier;

            if (gameTime >= 10)
            {
                gameTime = 0;
                months++;
                CallMonthEventChange();
                gameManagerMaster.CallOnMonthPassedEvent();
                if (months.Equals(12))
                {
                    months = 0;
                    years++;
                }
            }
        }
	}
}
