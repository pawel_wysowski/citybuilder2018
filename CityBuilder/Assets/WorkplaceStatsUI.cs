﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class WorkplaceStatsUI : MonoBehaviour, IMenuView
{
    public GameObject VerticalLayoutGroup;
    public TextMeshProUGUI VacancyLabel;
    public TextMeshProUGUI EmployeesLabel;
    public Button backButton;
    public Button destroyButton;
    private Sprite image;
    private Building _building;
    [SerializeField] private GameManagerMaster gameManager;
    private void OnEnable()
    {
        GameManagerBuilding.OnFactoryTileClicked += Show;
    }

    private void OnDisable()
    {
        GameManagerBuilding.OnFactoryTileClicked -= Show;
    }

    private void Show(Building building)
    {
        _building = building;
        backButton.onClick.RemoveAllListeners();
        backButton.onClick.AddListener(gameManager.Pop);
        destroyButton.onClick.AddListener(DestroyBuilding);
        ShowMenu();
    }

    private void Hide()
    {
        VerticalLayoutGroup.SetActive(false);
        GetComponent<Image>().enabled = false;
    }

    public void SetVacancyLabelLabel(int number)
    {
        VacancyLabel.SetText(string.Format("Unemployed: {0}", number));
    }

    public void SetEmployeesLabelLabel(int number)
    {
        EmployeesLabel.SetText(string.Format("Employed: {0}", number));
    }

    public void ShowMenu()
    {
        gameManager.Push(this);
        var factory = (BuildingFactory)_building;
        SetVacancyLabelLabel(factory.MaxEmployees - factory.CurrentEmployees);
        SetEmployeesLabelLabel(factory.CurrentEmployees);
        GetComponent<Image>().enabled = true;
        VerticalLayoutGroup.SetActive(true);
    }

    private void DestroyBuilding()
    {
        if (_building)
        {
            _building.tile.DestroyBuilding(_building);
            gameManager.Pop();
        }
    }

    public void HideMenu()
    {
        _building = null;
        Hide();
    }
}
