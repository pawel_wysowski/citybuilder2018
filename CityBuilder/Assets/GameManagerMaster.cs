﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManagerMaster : MonoBehaviour {

    public Stack<IMenuView> UisEnabled = new Stack<IMenuView>();
    private int _money=0;

    public int money { get { return _money; } set { if (_money != value) _money = value; CallOnMoneyChangedEvent(); } }

    public delegate void GameManagerMasterEventHandler();
    public event GameManagerMasterEventHandler OnGameStartEvent;
    public event GameManagerMasterEventHandler OnGameEndEvent;
    public event GameManagerMasterEventHandler OnMonthPassedEvent;

    public delegate void GameManagerMasterEventHandlerMoney(int money);
    public static event GameManagerMasterEventHandlerMoney OnMoneyChanged;

    public void CallOnGameStartEvent()
    {
        OnGameStartEvent?.Invoke();
    }

    public void CallOnGameEndEvent()
    {
        OnGameEndEvent?.Invoke();
    }

    public void CallOnMonthPassedEvent()
    {
        OnMonthPassedEvent?.Invoke();
    }

    public void CallOnMoneyChangedEvent()
    {
        OnMoneyChanged?.Invoke(_money);
    }

    public void Push(IMenuView menuView)
    {
        UisEnabled.Push(menuView);
        GetComponent<GameManagerClickInteraction>().shouldDetectInteraction = false;
    }

    public void Pop()
    {
        var menu = UisEnabled.Pop();
        menu.HideMenu();
        GetComponent<GameManagerClickInteraction>().shouldDetectInteraction = true;
    }

    // Use this for initialization
    void Start () {
        OnGameStartEvent();
        FindObjectOfType<SoundManager>().StartPlayingBackgroundSound();
        FindObjectOfType<QuestManager>().StartQuest(0);
        money += 1000;
	}

    public void TogglePause()
    {
        GetComponent<GameManagerPause>().TogglePause();
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
