﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManagerClickInteraction : MonoBehaviour, IGameManager
{
    private GameManagerMaster gameManagerMaster;
    public bool shouldDetectInteraction;
    private GameObject itemUnderCursor;

    public delegate void GameManagerClickInteractionEventHandler();
    public static event GameManagerClickInteractionEventHandler ClickOnBackground;

    public LayerMask layerMask;

    public void SetInitialReferences()
    {
        gameManagerMaster = GetComponent<GameManagerMaster>();
    }

    void OnEnable()
    {
        SetInitialReferences();
        gameManagerMaster.OnGameStartEvent += GameManagerMaster_OnGameStartEvent;

    }
    void OnDisable()
    {
        gameManagerMaster.OnGameStartEvent -= GameManagerMaster_OnGameStartEvent;
    }
    private void GameManagerMaster_OnGameStartEvent()
    {
        shouldDetectInteraction = true;
    }
    private void Interaction(Transform obj)
    {
        itemUnderCursor = obj.gameObject;

        if (Input.GetMouseButtonDown(0))
            obj.GetComponent<Tile>().ClickInteraction();
        else
            obj.GetComponent<Tile>().HoverInteraction();
    }

    // Update is called once per frame
    void Update()
    {
        if (shouldDetectInteraction)
        {
            RaycastHit hit;
            Vector3 scrPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
            Ray ray = Camera.main.ScreenPointToRay(scrPoint);
            if (Physics.Raycast(ray, out hit, 30, layerMask))
            {
                if (itemUnderCursor!= null && hit.transform != itemUnderCursor)
                {
                    itemUnderCursor.GetComponent<Tile>().HoverInteractionEnd();
                }
                Interaction(hit.transform);
            }
            else
            {
                ResetInteraction();
            }

        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameManagerMaster.UisEnabled.Any())
            {
                gameManagerMaster.Pop();
            }
            else
            {
                gameManagerMaster.TogglePause();
            }
        }
    }

    private void CallClickOnBackground()
    {
        ClickOnBackground?.Invoke();
    }

    private void ResetInteraction()
    {
        if (itemUnderCursor)
        {
            itemUnderCursor.GetComponent<Tile>().HoverInteractionEnd();
            itemUnderCursor = null;
        }

    }
}
