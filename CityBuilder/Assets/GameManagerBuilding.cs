﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerBuilding : MonoBehaviour
{

    public delegate void GameManagerBuildingEventHadler(int buildingWithoutPower);
    public static event GameManagerBuildingEventHadler OnBuildingsWithoutPowerChange;

    public delegate void GameManager_BuildingUIHandler(Building building);
    public static event GameManager_BuildingUIHandler OnHouseTileClicked;
    public static event GameManager_BuildingUIHandler OnFactoryTileClicked;
    public static event GameManager_BuildingUIHandler OnPowerplantTileClicked;
    public static event GameManager_BuildingUIHandler OnParkTileClicked;
    public static event GameManager_BuildingUIHandler OnPowerRepeaterTileClicked;

    public List<Building> BuildingModels;
    public List<GameObject> HousesOnMap = new List<GameObject>();
    public List<GameObject> WorkPlaces = new List<GameObject>();
    public List<GameObject> HappinessPlaces = new List<GameObject>();
    public List<GameObject> Powerplants = new List<GameObject>();
    public int BuildingsWithoutPower
    {
        get { return buildingsWithoutPower; }
        set
        {
            if (buildingsWithoutPower != value)
                buildingsWithoutPower = value;

            CallBuildingsWithoutPowerChange();
        }
    }


    private int buildingsWithoutPower = 0;

    public GameObject GetBuildingModel(BuildingTypeEnum buildingType)
    {
        var building = BuildingModels.Find(x => x.buildingType.Equals(buildingType));
        return building.buildingPrefab;
    }

    private void OnEnable()
    {
        Building.OnBuildingEnabled += Building_OnBuildingEnabled;
        Building.OnBuildingDisabled += Building_OnBuildingDisabled;
        Tile.OnBuildingClicked += ShowTileUI;
    }

    private void ShowTileUI(Building building)
    {
        if (building.GetComponent<BuildingHousing>())
        {
            OnHouseTileClicked?.Invoke(building);
        }
        else if (building.GetComponent<BuildingFactory>())
        {
            OnFactoryTileClicked?.Invoke(building);
        }
        else if (building.GetComponent<BuildingCoalPowerplant>())
        {
            OnPowerplantTileClicked?.Invoke(building);
        }
        else if(building.GetComponent<BuildingPark>())
        {
            OnParkTileClicked?.Invoke(building);
        }
        else if (building.GetComponent<BuildingPowerRepeater>())
        {
            OnPowerRepeaterTileClicked?.Invoke(building);
        }
    }

    private void OnDisable()
    {
        Building.OnBuildingEnabled -= Building_OnBuildingEnabled;
        Building.OnBuildingDisabled -= Building_OnBuildingDisabled;
        Tile.OnBuildingClicked -= ShowTileUI;
    }

    private void Building_OnBuildingEnabled(GameObject building)
    {
        building.GetComponent<Building>().gameManagerBuilding = this;
        if (building.GetComponent<BuildingHousing>())
            HousesOnMap.Add(building);
        else if (building.GetComponent<BuildingFactory>())
        {
            WorkPlaces.Add(building);
        }
        else if (building.GetComponent<BuildingPark>())
        {
            HappinessPlaces.Add(building);
        }
        else if (building.GetComponent<BuildingCoalPowerplant>())
        {
            Powerplants.Add(building);
        }
        CheckBuildingsWithoutPower();
        GetComponent<GameManagerMaster>().money -= building.GetComponent<Building>().cost;
    }

    private void Building_OnBuildingDisabled(GameObject building)
    {
        if (building.GetComponent<BuildingHousing>())
        {
            HousesOnMap.Remove(building);
            HousesOnMap.TrimExcess();
        }
        else if (building.GetComponent<BuildingFactory>())
        {
            WorkPlaces.Remove(building);
            WorkPlaces.TrimExcess();
        }
        else if (building.GetComponent<BuildingPark>())
        {
            HappinessPlaces.Remove(building);
            HappinessPlaces.TrimExcess();
        }
        else if (building.GetComponent<BuildingCoalPowerplant>())
        {
            Powerplants.Remove(building);
            Powerplants.TrimExcess();
        }
        GetComponent<GameManagerMaster>().money += building.GetComponent<Building>().cost/4;
        CheckBuildingsWithoutPower();
    }

    public void CheckBuildingsWithoutPower()
    {
        buildingsWithoutPower = 0;
        foreach(GameObject house in HousesOnMap)
        {
            if(!house.GetComponent<IHavePowerNeeds>().hasPower)
            {
                buildingsWithoutPower++;
            } 
        }
        foreach (GameObject workplace in WorkPlaces)
        {
            if (!workplace.GetComponent<IHavePowerNeeds>().hasPower)
            {
                buildingsWithoutPower++;
            }
        }
        BuildingsWithoutPower = buildingsWithoutPower;
    }

    private void CallBuildingsWithoutPowerChange()
    {
        OnBuildingsWithoutPowerChange?.Invoke(BuildingsWithoutPower);
    }
}
