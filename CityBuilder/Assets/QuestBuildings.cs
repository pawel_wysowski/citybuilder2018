﻿using UnityEngine;
using System.Collections;

public class QuestBuildings : Quest
{
    private GameObject QuestViewInst;
    private int buildingCounter;

    public int buildingCountObjective=0;
    public BuildingTypeEnum buildingObjective;

    public override void CompleteQuest()
    {
        Building.OnBuildingEnabled -= CheckObjective;
        IsCompleted = true;
        CallQuestCompletedEvent();
        QuestViewInst.GetComponent<QuestView>().SetQuestCompleteLabel();
    }

    public override void StartQuest()
    {
        QuestViewInst = Instantiate(QuestView, QuestUI);
        QuestViewInst.GetComponent<QuestView>().ShowQuestView(this);
        Building.OnBuildingEnabled += CheckObjective;
    }

    private void CheckObjective(GameObject _building)
    {
        Building building = _building.GetComponent<Building>();
        if (building.buildingType.Equals(buildingObjective))
        {
            buildingCounter++;
        }

        if (buildingCounter.Equals(buildingCountObjective))
        {
            CompleteQuest();
        }

    }

    public override void DestroyView()
    {
        Destroy(QuestViewInst);
    }
}
