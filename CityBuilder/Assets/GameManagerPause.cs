﻿using UnityEngine;
using System.Collections;

public class GameManagerPause : MonoBehaviour
{

    [SerializeField]
    private PauseMenuView pauseMenu;

    public void TogglePause()
    {
        if (!pauseMenu.visible)
        {
            pauseMenu.Show();
            Time.timeScale = 0;
        }
        else
        {
            pauseMenu.Hide();
            Time.timeScale = 1;
        }      
    }

}
