﻿using UnityEngine;

public abstract class Quest : MonoBehaviour
{
    public string QuestName;
    public string QuestDescription;
    public GameObject QuestView;
    public Transform QuestUI;
    public bool IsCompleted;
    public abstract void StartQuest();
    public abstract void CompleteQuest();
    public abstract void DestroyView();

    public delegate void QuestEventHandler(Quest quest);
    public static event QuestEventHandler OnQuestComplete;

    public void CallQuestCompletedEvent()
    {
        OnQuestComplete?.Invoke(this);
    }
}