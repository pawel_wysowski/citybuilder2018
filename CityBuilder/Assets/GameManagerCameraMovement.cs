﻿using UnityEngine;
using System.Collections;
using System;

public class GameManagerCameraMovement : MonoBehaviour
{
    private Vector3 prevPosition;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        ZoomInput();
        MoveCamera();
    }

    private void ZoomInput()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0 && Camera.main.orthographicSize > 1)
        {
            Camera.main.orthographicSize--;
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0 && Camera.main.orthographicSize < 11)
        {
            Camera.main.orthographicSize++;
        }
    }

    private void MoveCamera()
    {
        if (Input.GetMouseButtonDown(2))
        {
            prevPosition = Input.mousePosition;
        }
        if (Input.GetMouseButton(2))
        {
            var delta = Input.mousePosition-prevPosition;

            if (delta.x > 50)
            {
                var newPos = Camera.main.transform.position;
                newPos.x += 0.1f;
                Camera.main.transform.position = newPos;
            }
            else if(delta.x < -50)
            {
                var newPos = Camera.main.transform.position;
                newPos.x -= 0.1f;
                Camera.main.transform.position = newPos;
            }

            if (delta.y > 50)
            {
                var newPos = Camera.main.transform.position;
                newPos.y += 0.1f;
                Camera.main.transform.position = newPos;
            }
            else if (delta.y < -50)
            {
                var newPos = Camera.main.transform.position;
                newPos.y -= 0.1f;
                Camera.main.transform.position = newPos;
            }

        }
    }
}
