﻿using UnityEngine;
using System.Collections;

public class QuestHappinessHold : Quest
{
    [SerializeField]
    private GameManagerTime gameManager_Time;
    private GameObject QuestViewInst;
    private int happinessObjective;
    private int monthStart;
    public int months;
    private bool startCounting;

    public override void CompleteQuest()
    {
        GameManagerHappiness.OnHappinessStatChanged -= CheckObjective;
        GameManagerTime.MonthEventChange -= GameManager_Time_MonthEventChange;
        IsCompleted = true;
        CallQuestCompletedEvent();
        QuestViewInst.GetComponent<QuestView>().SetQuestCompleteLabel();
    }

    private void GameManager_Time_MonthEventChange(int year, int month)
    {
        int sum = year * 12 + month;
        if (sum - monthStart >= months && startCounting)
        {
            CompleteQuest();
        }
    }

    public override void StartQuest()
    {
        happinessObjective = 15;
        QuestViewInst = Instantiate(QuestView, QuestUI);
        QuestViewInst.GetComponent<QuestView>().ShowQuestView(this);
        GameManagerHappiness.OnHappinessStatChanged += CheckObjective;
        GameManagerTime.MonthEventChange += GameManager_Time_MonthEventChange;
    }

    private void CheckObjective(int happiness)
    {
        if (happiness >= happinessObjective && !startCounting)
        {
            startCounting = true;
            monthStart = gameManager_Time.GetMonth();
        }
        else if(happiness< happinessObjective && startCounting)
        {
            startCounting = false;
            monthStart = 0;
        }
    }

    public override void DestroyView()
    {
        Destroy(QuestViewInst);
    }
}

