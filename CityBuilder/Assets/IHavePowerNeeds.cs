﻿public interface IHavePowerNeeds 
{
    bool hasPower { get; set; }
    float powerNeeds { get; set; }
}
