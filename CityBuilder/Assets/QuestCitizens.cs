﻿using UnityEngine;
using System.Collections;

public class QuestCitizens : Quest
{
    [SerializeField]
    private GameManagerCitizens gameManager_Citizens;
    private GameObject QuestViewInst;
    private int citizensObjective;
    private bool shouldCheckObjective;
    public override void CompleteQuest()
    {
        IsCompleted = true;
        shouldCheckObjective = false;
        CallQuestCompletedEvent();
        QuestViewInst.GetComponent<QuestView>().SetQuestCompleteLabel();
    }

    public override void StartQuest()
    {
        citizensObjective = 200;
        QuestViewInst = Instantiate(QuestView, QuestUI);
        QuestViewInst.GetComponent<QuestView>().ShowQuestView(this);
        shouldCheckObjective = true;
    }

    private void CheckObjective()
    {
        if (gameManager_Citizens.AllCitizens >= citizensObjective)
        {
            CompleteQuest();
        }
    }

    public override void DestroyView()
    {
        Destroy(QuestViewInst);
    }

    private void Update()
    {
        if (shouldCheckObjective)
        {
            CheckObjective();
        }
    }
}
