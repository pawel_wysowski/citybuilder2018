﻿public interface IMenuView
{
    void ShowMenu();
    void HideMenu();
}