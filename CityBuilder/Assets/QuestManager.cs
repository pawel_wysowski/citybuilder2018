﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestManager : MonoBehaviour
{
    public List<Quest> Quests;
    public List<Quest> QuestsLvl1;
    public List<Quest> QuestsLvl2;
    public List<Quest> QuestsLvl3;
    public List<Quest> QuestsLvl4;
    public List<Quest> QuestsLvl5;
    public List<Quest> QuestsLvl6;
    public List<Quest> QuestsLvl7;

    public delegate void QuestManagerEventHandler(int lvl);
    public static event QuestManagerEventHandler OnLevelUp;

    public int Level { get { return level; } set { if (level != value) level = value; OnLevelUp?.Invoke(Level); } }
    private int level = 0;

    private void OnEnable()
    {
        Quest.OnQuestComplete += CheckIfAllLevelQuestCompleted;
    }

    private void OnDisable()
    {
        Quest.OnQuestComplete -= CheckIfAllLevelQuestCompleted;
    }

    private void CheckIfAllLevelQuestCompleted(Quest quest)
    {
        Quest questUncompleted = null;
        if (level.Equals(0))
        {
            questUncompleted = QuestsLvl1.Find(q => !q.IsCompleted);
        }
        else if (level.Equals(1))
        {
            questUncompleted = QuestsLvl1.Find(q => !q.IsCompleted);
        }
        else if (level.Equals(2))
        {
            questUncompleted = QuestsLvl1.Find(q => !q.IsCompleted);
        }
        else if (level.Equals(3))
        {
            questUncompleted = QuestsLvl1.Find(q => !q.IsCompleted);
        }
        else if (level.Equals(4))
        {
            questUncompleted = QuestsLvl1.Find(q => !q.IsCompleted);
        }
        else if (level.Equals(5))
        {
            questUncompleted = QuestsLvl1.Find(q => !q.IsCompleted);
        }
        else if (level.Equals(6))
        {
            questUncompleted = QuestsLvl1.Find(q => !q.IsCompleted);
        }
        else if (level.Equals(7))
        {
            questUncompleted = QuestsLvl1.Find(q => !q.IsCompleted);
        }

        if (questUncompleted == null)
        {
            Level++;
            DestroyQuestsForLvl(Level-1);
            StartQuest(Level);
            return;
        }
    }

    private void DestroyQuestsForLvl(int lvl)
    {
        if (lvl.Equals(0))
        {
            foreach(Quest quest in QuestsLvl1)
            {
                quest.DestroyView();
            }
        }
        else if(lvl.Equals(1))
        {
            foreach (Quest quest in QuestsLvl2)
            {
                quest.DestroyView();
            }
        }
        else if (lvl.Equals(2))
        {
            foreach (Quest quest in QuestsLvl3)
            {
                quest.DestroyView();
            }
        }
        else if (lvl.Equals(3))
        {
            foreach (Quest quest in QuestsLvl4)
            {
                quest.DestroyView();
            }
        }
        else if (lvl.Equals(4))
        {
            foreach (Quest quest in QuestsLvl5)
            {
                quest.DestroyView();
            }
        }
        else if (lvl.Equals(5))
        {
            foreach (Quest quest in QuestsLvl6)
            {
                quest.DestroyView();
            }
        }
        else if (lvl.Equals(6))
        {
            foreach (Quest quest in QuestsLvl7)
            {
                quest.DestroyView();
            }
        }
    }

    public void StartQuest(int i)
    {
        if (level.Equals(0))
        {
            foreach(Quest q in QuestsLvl1)
            {
                q.StartQuest();
            }
        }
        else if (level.Equals(1))
        {
            foreach(Quest q in QuestsLvl2)
            {
                q.StartQuest();
            }
        }
        else if (level.Equals(2))
        {
            foreach (Quest q in QuestsLvl3)
            {
                q.StartQuest();
            }
        }
        else if (level.Equals(3))
        {
            foreach (Quest q in QuestsLvl4)
            {
                q.StartQuest();
            }
        }
        else if (level.Equals(4))
        {
            foreach (Quest q in QuestsLvl5)
            {
                q.StartQuest();
            }
        }
        else if (level.Equals(5))
        {
            foreach (Quest q in QuestsLvl6)
            {
                q.StartQuest();
            }
        }
        else if (level.Equals(6))
        {
            foreach (Quest q in QuestsLvl7)
            {
                q.StartQuest();
            }
        }
    }
}
