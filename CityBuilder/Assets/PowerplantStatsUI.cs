﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PowerplantStatsUI : MonoBehaviour, IMenuView
{
    public GameObject VerticalLayoutGroup;
    public TextMeshProUGUI PowerLabel;
    public TextMeshProUGUI HappinessReductionLabel;
    public Button backButton;
    public Button destroyButton;
    private Sprite image;
    private Building _building;
    [SerializeField] private GameManagerMaster gameManager;
    private void OnEnable()
    {
        GameManagerBuilding.OnPowerplantTileClicked += Show;
    }

    private void OnDisable()
    {
        GameManagerBuilding.OnPowerplantTileClicked -= Show;
    }

    private void Show(Building building)
    {
        _building = building;
        backButton.onClick.RemoveAllListeners();
        backButton.onClick.AddListener(gameManager.Pop);
        destroyButton.onClick.AddListener(DestroyBuilding);
        ShowMenu();
    }

    private void Hide()
    {
        VerticalLayoutGroup.SetActive(false);
        GetComponent<Image>().enabled = false;
    }

    public void SetPowerLabel(int number)
    {
        PowerLabel.SetText(string.Format("Power produced: {0}", number));
    }

    public void SetHappinessReductionLabel(int number)
    {
        HappinessReductionLabel.SetText(string.Format("Happiness modifier: {0}", -number));
    }

    public void ShowMenu()
    {
        gameManager.Push(this);
        var powerplant = (BuildingCoalPowerplant)_building;
        SetHappinessReductionLabel(-powerplant.HappinessModifier);
        SetPowerLabel((int)powerplant.powerSupply);
        GetComponent<Image>().enabled = true;
        VerticalLayoutGroup.SetActive(true);
    }

    private void DestroyBuilding()
    {
        if (_building)
        {
            _building.tile.DestroyBuilding(_building);
            gameManager.Pop();
        }
    }

    public void HideMenu()
    {
        _building = null;
        Hide();
    }
}
