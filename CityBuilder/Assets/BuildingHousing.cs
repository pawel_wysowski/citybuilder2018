﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class BuildingHousing : Building, IHavePowerNeeds
{
    private int _citizensNumber = 0;
    public float powerNeed;

    public List<Citizen> CitizensList = new List<Citizen>();
    public int Citizens
    {
        get
        {
            return _citizensNumber;
        }
        set
        {
            if (_citizensNumber != value)
            {
                _citizensNumber = value;
                CallCitizensChangedEvent();
            }
        }
    }


    // public int UnemployedCitizens;
    public bool hasPower
    {
        get
        {
            return _hasPower;
        }

        set
        {
            if (_hasPower != value)
            {
                _hasPower = value;
            }
        }
    }

    public float powerNeeds
    {
        get
        {
            return powerNeed;
        }
        set
        {
            if (powerNeed != value)
                powerNeed = value;
        }
    }
    [SerializeField]
    private bool _hasPower;
    [SerializeField]
    private int MaxCitizens;
    private bool buildingEnabled;

    public delegate void BuildingHousingEventHandler();
    public static event BuildingHousingEventHandler OnCitizensChanged;

    public override void RegisterBuilding()
    {
        hasPower = false;
        base.buildingType = BuildingTypeEnum.Housing;
        base.HappinessModifier = 0;
        CallOnBuildingBuild(gameObject);
        int randomNumber = UnityEngine.Random.Range(2, 11);
        Citizens = randomNumber;
        GenerateCitizensObjects(Citizens);

    }

    public override void UnregisterBuilding()
    {
        hasPower = false;
        tile.power += powerNeeds;
        //CallOnBuildingDestroyed(gameObject);
        RemoveAllCitizens();
        buildingEnabled = false;
        CallOnBuildingDestroyed(gameObject);
        //Destroy(gameObject);
    }

    private void RemoveAllCitizens()
    {
        // int counter = CitizensList.Count;
        for (int i = 0; i < CitizensList.Count; i++)
        {
            var citizen = CitizensList[i];
            if (citizen.haveJob)
                citizen.DismissFromWork();
            else
                DestroyCitizen(citizen);

            citizen = null;
            // CitizensList.RemoveAt(i);
        }
        CitizensList.Clear();
        Citizens = CitizensList.Count;
    }

    private void GenerateCitizensObjects(int citizensToCreate)
    {
        for (int i = 0; i < citizensToCreate; i++)
        {
            CitizensList.Add(new Citizen(false, null, this));
        }
    }

    public override void ApplyBuildingModifiers(Tile tile)
    {
        CheckIfWorkplaceAvailable(tile);
    }

    private void CheckIfWorkplaceAvailable(Tile tile)
    {
        var buildingOnTile = tile.GetBuilding();
        if (buildingOnTile != null)
        {
            var work = buildingOnTile.GetComponent<IHaveEmployees>();

            if (work != null)
            {
                UnemployedCitizens(work);
            }
        }
    }

    public void MoveIn()
    {
        if (Citizens < MaxCitizens)
        {
            Citizen citizen = new Citizen(false, null, this);
            CitizensList.Add(citizen);
            Citizens = CitizensList.Count;
            CheckIfWorkplaceAvailable(tile);
            tile.ApplyBuildingEffects(this);
        }
    }

    public void UnemployedCitizens(IHaveEmployees work)
    {
        foreach (Citizen citizen in CitizensList)
        {
            if (!citizen.haveJob)
            {
                work.HireEmployee(citizen);
            }
        }
    }

    public void MoveOut()
    {
        if (CitizensList.Any())
        {
            var citizen = CitizensList[CitizensList.Count - 1];
            if (citizen.haveJob)
                citizen.DismissFromWork();
            DestroyCitizen(citizen);
        }
        Citizens = CitizensList.Count;
    }

    public void DestroyCitizen(Citizen citizen)
    {
        CitizensList.Remove(citizen);
        CitizensList.TrimExcess();
    }

    public int UnemployedCitizensCount()
    {
        return CitizensList.FindAll(x => !x.haveJob).Count;
    }

    public int EmployedCitizensCount()
    {
        return CitizensList.FindAll(x => x.haveJob).Count;
    }

    public int AllCitizensCount()
    {
        return Citizens;
    }

    public override void ApplyBuildingModifiersForOneTile(Tile tile)
    {
        if (!buildingEnabled)
            tile.power -= powerNeed;
        buildingEnabled = true;
    }


    private void CallCitizensChangedEvent()
    {
        OnCitizensChanged?.Invoke();
    }

    public override void ApplyBuildingModifiersOnDestroy(Tile tile)
    {

    }
}
