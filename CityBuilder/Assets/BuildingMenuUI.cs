﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingMenuUI : MonoBehaviour, IMenuView {
    [SerializeField] private GameObject panel;
    [SerializeField] private GameManagerMaster gameManager;
    [SerializeField] private Button housingBtn;
    [SerializeField] private Button powerplantBtn;
    [SerializeField] private Button industyBtn;
    [SerializeField] private Button powerReapeaterBtn;
    [SerializeField] private Button parkBtn;

    private Tile selectedTile;
    private bool shouldShowBtnsLvl2;

    public delegate void BuildingMenuUIHandler();
    public static event BuildingMenuUIHandler MoneyShortage;

    private void AddListenersToBtns()
    {
        housingBtn.onClick.AddListener(()=> { SelectBuilding(BuildingTypeEnum.Housing); });
        powerplantBtn.onClick.AddListener(() => { SelectBuilding(BuildingTypeEnum.PowerPlants); });
        industyBtn.onClick.AddListener(() => { SelectBuilding(BuildingTypeEnum.Industries); });
        powerReapeaterBtn.onClick.AddListener(() => { SelectBuilding(BuildingTypeEnum.PowerRepeater); });
        parkBtn.onClick.AddListener(() => { SelectBuilding(BuildingTypeEnum.Parks); });
    }

    private void RemoveAllListenersFromBtns()
    {
        housingBtn.onClick.RemoveAllListeners();
        powerplantBtn.onClick.RemoveAllListeners();
        industyBtn.onClick.RemoveAllListeners();
        parkBtn.onClick.RemoveAllListeners();
        powerReapeaterBtn.onClick.RemoveAllListeners();
    }

    private void OnEnable()
    {
        Tile.OnTileClicked += TileClicked;
        QuestManager.OnLevelUp += QuestManager_OnLevelUp;
        AddListenersToBtns();
        ShowLvlOneBtns();
    }

    private void QuestManager_OnLevelUp(int lvl)
    {
        if (lvl > 1)
        {
            ShowPowerRepeater();
        }
    }

    private void ShowPowerRepeater()
    {
        shouldShowBtnsLvl2 = true;
    }

    private void ShowLvlOneBtns()
    {
        housingBtn.gameObject.SetActive(true);
        powerplantBtn.gameObject.SetActive(true);
        industyBtn.gameObject.SetActive(true);
        parkBtn.gameObject.SetActive(true);
        powerReapeaterBtn.gameObject.SetActive(true);
        if (shouldShowBtnsLvl2)
        powerReapeaterBtn.gameObject.SetActive(true);
    }

    private void SelectBuilding(BuildingTypeEnum type)
    {
        Debug.Log(type);
        if (selectedTile)
        {
            var building = FindObjectOfType<GameManagerBuilding>().BuildingModels.Find(x => x.buildingType.Equals(type));
            Debug.Log(building);
            if (building)
            {
                if (gameManager.money >= building.cost)
                    selectedTile.Build(building);
                else
                    CallOnMoneyShortage();
            }
        }
        gameManager.Pop();
    }

    private void TileClicked(Tile tile)
    {
        ShowMenu();
        selectedTile = tile;
    }

    private void CallOnMoneyShortage()
    {
        if(MoneyShortage!=null)
        {
            MoneyShortage();
        }
    }

    private void ShowPanel()
    {
        panel.SetActive(true);
    }

    private void HidePanel()
    {
        panel.SetActive(false);
        selectedTile = null;
    }

    private void OnDisable()
    {
        Tile.OnTileClicked -= TileClicked;
        QuestManager.OnLevelUp -= QuestManager_OnLevelUp;
        RemoveAllListenersFromBtns();
    }

    public void HideMenu()
    {
        HidePanel();
    }

    public void ShowMenu()
    {
        ShowPanel();
        gameManager.Push(this);
    }
}
