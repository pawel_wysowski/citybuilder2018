﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class QuestHappiness : Quest
{
    private GameObject QuestViewInst;
    private int happinessObjective;
    public override void CompleteQuest()
    {
        GameManagerHappiness.OnHappinessStatChanged -= CheckObjective;
        IsCompleted = true;
        CallQuestCompletedEvent();
        QuestViewInst.GetComponent<QuestView>().SetQuestCompleteLabel();
    }

    public override void StartQuest()
    {
        happinessObjective = 30;
        QuestViewInst = Instantiate(QuestView, QuestUI);
        QuestViewInst.GetComponent<QuestView>().ShowQuestView(this);
        GameManagerHappiness.OnHappinessStatChanged += CheckObjective;
    }

    private void CheckObjective(int happiness)
    {
        if (happiness >= happinessObjective)
        {
            CompleteQuest();
        }
    }

    public override void DestroyView()
    {
        Destroy(QuestViewInst);
    }
}
